#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("echo", help="echo the string you use here")
parser.add_argument("name", help="name to be included in message")
args = parser.parse_args()
print(args.echo)
print("Hello,", args.name)

#!/usr/bin/env python3
# program to generate list of moves to solve a 15 puzzle

# test puzzle to be replaced by input
# testlist = ['14',' 3','13','11','10',' 6','12','15',' 2','  ',' 9',' 5',' 1',' 8',' 7',' 4']
testlist = [
    "14",
    " 3",
    "13",
    "11",
    "10",
    " 6",
    "12",
    "15",
    " 2",
    "  ",
    " 9",
    " 5",
    " 1",
    " 8",
    " 7",
    " 4",
]


# define valid moves for each position on grid
rules = {
    0: [1, 4],
    1: [0, 2, 5],
    2: [1, 3, 6],
    3: [2, 7],
    4: [0, 5, 8],
    5: [1, 4, 6, 9],
    6: [2, 5, 7, 10],
    7: [3, 6, 11],
    8: [4, 9, 12],
    9: [5, 8, 10, 13],
    10: [6, 9, 11, 14],
    11: [7, 10, 15],
    12: [8, 13],
    13: [9, 12, 14],
    14: [10, 13, 15],
    15: [11, 14],
}

# Define values in same rows and cols
rows = {1: [0, 1, 2, 3], 2: [4, 5, 6, 7], 3: [8, 9, 10, 11], 4: [12, 13, 14, 15]}

cols = {1: [0, 4, 8, 12], 2: [1, 5, 9, 13], 3: [2, 6, 10, 14], 4: [3, 7, 11, 15]}

# working list for solution
workinglist = testlist

# Solve for moving 1 to target location

# static target value for dev
targetvalue = "1"

# reset location holders(index, row, col) for current location of the target number,
# location of the target destination for target number, and current location of
# blank(open) space
targetdestindex = 0
targetdestcol = 0
targetdestrow = 0
blankloc = 0
blankrow = 0
blankcol = 0
targetloc = 0
targetrow = 0
targetcol = 0

# Find current index location for the blank and target number
for i in range(0, 16):
    if workinglist[i] == " 1":
        targetloc = i
    if workinglist[i] == "  ":
        blankloc = i


# Find row and col location for the blank, target number, and
# target number destination

if targetdestindex != targetloc:
    for i in range(1, 5):
        if targetloc in rows[i]:
            targetrow = i
        if targetloc in cols[i]:
            targetcol = i
        if blankloc in rows[i]:
            blankrow = i
        if blankloc in cols[i]:
            blankcol = i
        if targetdestindex in rows[i]:
            targetdestrow = i
        if targetdestindex in cols[i]:
            targetdestcol = i

    # Determine directions target and blank need to move in path to target dest

    trowmove = " "
    tcolmove = " "
    browmove = " "
    bcolmove = " "

    # Does the target need to move up or down to get to destination
    if targetdestrow > targetrow:
        trowmove = "U"
    elif targetdestrow < targetrow:
        trowmove = "D"
    else:
        trowmove = "N"

    # Does target need to move left or right to get to destination
    if targetdestcol > targetcol:
        tcolmove = "R"
    elif targetdestcol < targetcol:
        tcolmove = "L"
    else:
        tcolmove = "N"

    # Does blank need to move up or down to position for target path
    if targetrow > blankrow:
        browmove = "D"
    elif targetrow < blankrow:
        browmove = "U"
    else:
        browmove = "N"

    # Does blank need to move left or right to position for target path
    if targetcol > blankcol:
        bcolmove = "R"
    elif targetcol < blankcol:
        bcolmove = "L"
    else:
        bcolmove = "N"

    #   if tcolmove == "N":

    if bcolmove != "N":
        if bcolmove == "L":
            movemod = -1
        else:
            movemod = 1
        movecelllist = cols[blankcol]
        movecell = movecelllist[blankcol + movemod]

    print(targetcol, targetrow, blankcol, blankrow, bcolmove, browmove)
    print("move cell " + str(movecell))

#!/usr/bin/env python3

list1 = []
managelist = True
removewords = False
requestwords = []
requestfull = " "

while managelist:

    request = input(
        """Enter a word to add to list, prefix with '-' to remove from list or enter 'q' to quit. """
    )

    if request.lower().strip() == "q":
        managelist = False
        break

    if request[0] == "-":
        removewords = True
        requestfull = request.strip("-")
    else:
        requestfull = request

    requestwords = requestfull.split()

    for word in requestwords:
        if removewords:
            list1.remove(word)
        else:
            list1.append(word)

    print(list1)
#python script for finding the largest palindrome product of 3 digit numbers

#product variable to use for palindrom evaluation
num3=0

#list of identified palindromes used to identify largest palindrome value
pallist=[]

#loop through 100 - 1000 for first number in multiplication product
for num1 in range(100,999):

#loop through 100 - 1000 for the second number in multiplication product
    for num2 in range(100,999):
        num3=num1*num2

#convert product to string to compare characters
        num3str=str(num3)

#set flag to determine if result passes check for palindrome
        palindrome=True

#determine length of product to establish number of loop iterations for checking for palindrome
        endrange=len(num3str)//2

#loop for checking product character by character for palindrome 
        for cntr in range (1,endrange+1):
#            print(cntr-1)
#            print(len(num3str)-cntr)
#break loop if confirmed product isn't a palindrom
            if num3str[cntr-1]!=num3str[len(num3str)-cntr]:
                palindrome=False
                break

#add identified palindrome to list to allow identification of max value palindrome
        if palindrome:
            pallist.append(num3)

#identify and display max palindrome from pal list
maxvalue=max(pallist)
print(maxvalue)


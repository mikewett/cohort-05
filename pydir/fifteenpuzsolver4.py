#!/usr/bin/env python3
# program to generate
# list of moves to solve a 15 puzzle

# Direction constants
UP = "up"
DOWN = "down"
LEFT = "left"
RIGHT = "right"


# puzzle grid to be solved(will be replaced by input method)
testdict = {
    (1, 1): "14",
    (1, 2): "  ",
    (1, 3): "13",
    (1, 4): "11",
    (2, 1): "10",
    (2, 2): " 3",
    (2, 3): "12",
    (2, 4): "15",
    (3, 1): " 2",
    (3, 2): " 6",
    (3, 3): " 9",
    (3, 4): " 4",
    (4, 1): " 5",
    (4, 2): " 8",
    (4, 3): " 7",
    (4, 4): " 1",
}
# Dictionary defines a solved grid
solvedict = {
    (1, 1): " 1",
    (1, 2): " 2",
    (1, 3): " 3",
    (1, 4): " 4",
    (2, 1): " 5",
    (2, 2): " 6",
    (2, 3): " 7",
    (2, 4): " 8",
    (3, 1): " 9",
    (3, 2): "10",
    (3, 3): "11",
    (3, 4): "12",
    (4, 1): "13",
    (4, 2): "14",
    (4, 3): "15",
    (4, 4): "  ",
}


def printgrid(currentgrid):
    """Takes dictionary of current game and generates a text based grid representation of the
    grid positions of each value

    Args:
        currentgrid (dict): dictionary of coordinate locations of each value on puzzle grid
    """
    print("-----------------")
    print(
        f"{currentgrid[(1,1)]} : {currentgrid[(1,2)]} : {currentgrid[(1,3)]} : {currentgrid[(1,4)]}"
    )
    print("-----------------")
    print(
        f"{currentgrid[(2,1)]} : {currentgrid[(2,2)]} : {currentgrid[(2,3)]} : {currentgrid[(2,4)]}"
    )
    print("-----------------")
    print(
        f"{currentgrid[(3,1)]} : {currentgrid[(3,2)]} : {currentgrid[(3,3)]} : {currentgrid[(3,4)]}"
    )
    print("-----------------")
    print(
        f"{currentgrid[(4,1)]} : {currentgrid[(4,2)]} : {currentgrid[(4,3)]} : {currentgrid[(4,4)]}"
    )
    print("-----------------")
    print("\n")


# functions to manage the mechanics of moving around the grid
# accepts current grid config and the location to be moved up
def moveup(currentgrid, gridloc):
    """Executes up grid move of a single value on the grid.  Moves selected value to space above current location
    on grid and places the value previously in that space to the provided location of the moved value.

    Args:
        currentgrid (dict): dictionary representing current grid location of all values
        gridloc (tuple): grid coordinates of value being moved
    """
    workinggridloc = ()
    workinggridloc = (gridloc[0] - 1, gridloc[1])
    val1 = currentgrid[gridloc]
    val2 = currentgrid[workinggridloc]
    currentgrid[gridloc] = val2
    currentgrid[workinggridloc] = val1


# accepts current grid config and the location to be moved down
def movedown(currentgrid, gridloc):
    """Executes down grid move of a single value on the grid.  Moves selected value to space above current location
    on grid and places the value previously in that space to the provided location of the moved value.

    Args:
        currentgrid (dict): dictionary representing current grid location of all values
        gridloc (tuple): grid coordinates of value being moved
    """
    workinggridloc = ()
    workinggridloc = (gridloc[0] + 1, gridloc[1])
    val1 = currentgrid[gridloc]
    val2 = currentgrid[workinggridloc]
    currentgrid[gridloc] = val2
    currentgrid[workinggridloc] = val1


# accepts current grid config and the location to be moved right
def moveright(currentgrid, gridloc):
    """Executes right grid move of a single value on the grid.  Moves selected value to space above current location
    on grid and places the value previously in that space to the provided location of the moved value.

    Args:
        currentgrid (dict): dictionary representing current grid location of all values
        gridloc (tuple): grid coordinates of value being moved
    """
    workinggridloc = ()
    workinggridloc = (gridloc[0], gridloc[1] + 1)
    val1 = currentgrid[gridloc]
    val2 = currentgrid[workinggridloc]
    currentgrid[gridloc] = val2
    currentgrid[workinggridloc] = val1


# accepts current grid config and the location to be moved left
def moveleft(currentgrid, gridloc):
    """Executes up grid move of a single value on the grid.  Moves selected value to space above current location
    on grid and places the value previously in that space to the provided location of the moved value.

    Args:
        currentgrid (dict): dictionary representing current grid location of all values
        gridloc (tuple): grid coordinates of value being moved
    """
    workinggridloc = ()
    workinggridloc = (gridloc[0], gridloc[1] - 1)
    val1 = currentgrid[gridloc]
    val2 = currentgrid[workinggridloc]
    currentgrid[gridloc] = val2
    currentgrid[workinggridloc] = val1


# Function to move grid locations on grid
def move(currentgrid, gridloc, direction):
    # calculate target location of move parm
    workinggridloc = ()
    if direction == UP:
        workinggridloc = (gridloc[0] - 1, gridloc[1])
    elif direction == DOWN:
        workinggridloc = (gridloc[0] + 1, gridloc[1])
    elif direction == LEFT:
        workinggridloc = (gridloc[0], gridloc[1] - 1)
    elif direction == RIGHT:
        workinggridloc = (gridloc[0], gridloc[1] + 1)

    # swap values of provided location and location indicated by direction parm
    val1 = currentgrid[gridloc]
    val2 = currentgrid[workinggridloc]
    currentgrid[gridloc] = val2
    currentgrid[workinggridloc] = val1


# handles specific scenario of repositioning blank space from behind target to in front of target.
def movearound(currentgrid, direction):
    #    targetloc = findgridloc(currentgrid, targetvalue)
    if direction == UP:
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, RIGHT)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, UP)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, UP)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, LEFT)
    if direction == DOWN:
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, RIGHT)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, DOWN)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, DOWN)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, LEFT)
    if direction == LEFT:
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, UP)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, LEFT)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, LEFT)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, DOWN)
    if direction == RIGHT:
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, UP)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, RIGHT)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, RIGHT)
        blankloc = findgridloc(currentgrid, "  ")
        move(currentgrid, blankloc, DOWN)


# Will look up current location of provided value on game grid
def findgridloc(currentgrid, targetvalue):
    targetlocation = tuple()
    for key, value in currentgrid.items():
        if value == targetvalue:
            targetlocation = key
    return targetlocation


# routine to move blank from anywhere in grid to location next to the move target value
def initializeblank(currentgrid, targetvalue):
    (
        goalrowdif,
        goalcoldif,
        blankrowdif,
        blankcoldif,
        targetloc,
        blankloc,
        goalloc,
    ) = findmovedistances(currentgrid, targetvalue)

    for i in range(0, abs(blankcoldif) - 1):
        if blankcoldif > 0:
            move(currentgrid, blankloc, LEFT)
            blankloc = (blankloc[0], blankloc[1] - 1)
        elif blankcoldif < 0:
            move(currentgrid, blankloc, RIGHT)
            blankloc = (blankloc[0], blankloc[1] + 1)

    for i in range(0, abs(blankrowdif) - 1):
        if blankrowdif < 0:
            move(currentgrid, blankloc, DOWN)
            blankloc = (blankloc[0] + 1, blankloc[1])
        elif blankrowdif > 0:
            move(currentgrid, blankloc, UP)
            blankloc = (blankloc[0] - 1, blankloc[1])


def findmovedistances(currentgrid, targetvalue, goalvalue=" "):
    targetloc = findgridloc(currentgrid, targetvalue)
    blankloc = findgridloc(currentgrid, "  ")
    if goalvalue == " ":
        goalloc = findgridloc(solvedict, targetvalue)
    else:
        goalloc = findgridloc(solvedict, goalvalue)
    goalrowdif = goalloc[0] - targetloc[0]
    goalcoldif = goalloc[1] - targetloc[1]
    blankrowdif = blankloc[0] - targetloc[0]
    blankcoldif = blankloc[1] - targetloc[1]
    return (
        goalrowdif,
        goalcoldif,
        blankrowdif,
        blankcoldif,
        targetloc,
        blankloc,
        goalloc,
    )


def findblankdata(currentgrid, targetvalue):
    blankloc = findgridloc(currentgrid, "  ")
    targetloc = findgridloc(currentgrid, targetvalue)
    blankrowdif = blankloc[0] - targetloc[0]
    blankcoldif = blankloc[1] - targetloc[1]
    return (blankloc, blankrowdif, blankcoldif, targetloc)


def findgoaldata(currentgrid, targetvalue, goalvalue=" "):
    targetloc = findgridloc(currentgrid, targetvalue)
    if goalvalue == " ":
        goalloc = findgridloc(solvedict, targetvalue)
    else:
        goalloc = findgridloc(solvedict, goalvalue)
    goalrowdif = goalloc[0] - targetloc[0]
    goalcoldif = goalloc[1] - targetloc[1]
    return (
        goalrowdif,
        goalcoldif,
        targetloc,
        goalloc,
    )


def movenumber(currentgrid, targetvalue):
    (
        goalrowdif,
        goalcoldif,
        blankrowdif,
        blankcoldif,
        targetloc,
        blankloc,
        goalloc,
    ) = findmovedistances(currentgrid, targetvalue)
    if goalcoldif > 0:
        horizontalmove(currentgrid, targetvalue, RIGHT, abs(goalcoldif))
    elif goalcoldif < 0:
        horizontalmove(currentgrid, targetvalue, LEFT, abs(goalcoldif))


def horizontalmove(currentgrid, targetvalue, direction, length):
    #    for i in range(0, length):
    (
        goalrowdif,
        goalcoldif,
        blankrowdif,
        blankcoldif,
        targetloc,
        blankloc,
        goalloc,
    ) = findmovedistances(currentgrid, targetvalue)

    while goalloc[1] != targetloc[1]:
        (
            goalrowdif,
            goalcoldif,
            blankrowdif,
            blankcoldif,
            targetloc,
            blankloc,
            goalloc,
        ) = findmovedistances(currentgrid, targetvalue)
        if goalloc[1] == targetloc[1]:
            continue
        # check if blank in right position and position if not
        # special case if blank is on opp side of proper position, move around
        if direction == LEFT:
            if blankcoldif == 1 and blankrowdif == 0 and targetloc[1] != 1:
                movearound(currentgrid, LEFT)
            if blankcoldif == 1 and blankrowdif == 0 and targetloc[1] == 1:
                move(currentgrid, blankloc, UP)
                blankloc = (blankloc[0] - 1, blankloc[1])
                move(currentgrid, blankloc, LEFT)
            if blankcoldif > -1 and blankrowdif != 0:
                move(currentgrid, blankloc, LEFT)
            if blankcoldif == -1 and blankrowdif > 0:
                move(currentgrid, blankloc, UP)
            if blankcoldif == -1 and blankrowdif < 0:
                move(currentgrid, blankloc, DOWN)

            # move target
            if blankcoldif == -1 and blankrowdif == 0:
                move(currentgrid, targetloc, LEFT)

        if direction == RIGHT:
            if blankcoldif == -1 and blankrowdif == 0:
                movearound(currentgrid, RIGHT)
            if blankcoldif < 1 and blankrowdif != 0:
                move(currentgrid, blankloc, RIGHT)
            if blankcoldif == 1 and blankrowdif > 0:
                move(currentgrid, blankloc, UP)
            if blankcoldif == 1 and blankrowdif < 0:
                move(currentgrid, blankloc, DOWN)

            # move target
            if blankcoldif == 1 and blankrowdif == 0:
                move(currentgrid, targetloc, RIGHT)

    # def verticalmove():
    (
        goalrowdif,
        goalcoldif,
        blankrowdif,
        blankcoldif,
        targetloc,
        blankloc,
        goalloc,
    ) = findmovedistances(currentgrid, targetvalue)

    while goalloc[1] != targetloc[1]:
        (
            goalrowdif,
            goalcoldif,
            blankrowdif,
            blankcoldif,
            targetloc,
            blankloc,
            goalloc,
        ) = findmovedistances(currentgrid, targetvalue)
        if goalloc[1] == targetloc[1]:
            continue
        # check if blank in right position and position if not
        # special case if blank is on opp side of proper position, move around
        if direction == UP:
            if blankcoldif == 1 and blankrowdif == 0 and targetloc[1] != 1:
                movearound(currentgrid, LEFT)
            if blankcoldif == 1 and blankrowdif == 0 and targetloc[1] == 1:
                move(currentgrid, blankloc, UP)
                blankloc = (blankloc[0] - 1, blankloc[1])
                move(currentgrid, blankloc, LEFT)
            if blankcoldif > -1 and blankrowdif != 0:
                move(currentgrid, blankloc, LEFT)
            if blankcoldif == -1 and blankrowdif > 0:
                move(currentgrid, blankloc, UP)
            if blankcoldif == -1 and blankrowdif < 0:
                move(currentgrid, blankloc, DOWN)

            # move target
            if blankcoldif == -1 and blankrowdif == 0:
                move(currentgrid, targetloc, UP)

        if direction == DOWN:
            if blankcoldif == -1 and blankrowdif == 0:
                movearound(currentgrid, RIGHT)
            if blankcoldif < 1 and blankrowdif != 0:
                move(currentgrid, blankloc, RIGHT)
            if blankcoldif == 1 and blankrowdif > 0:
                move(currentgrid, blankloc, UP)
            if blankcoldif == 1 and blankrowdif < 0:
                move(currentgrid, blankloc, DOWN)

            # move target
            if blankcoldif == 1 and blankrowdif == 0:
                move(currentgrid, targetloc, DOWN)


# printgrid(testdict)
# move(testdict, (3, 2), UP)
# printgrid(testdict)
# move(testdict, (1, 2), DOWN)
# printgrid(testdict)
# move(testdict, (1, 1), RIGHT)
# printgrid(testdict)
# move(testdict, (1, 2), LEFT)

printgrid(testdict)
initializeblank(testdict, " 1")
printgrid(testdict)
movenumber(testdict, " 1")
printgrid(testdict)
# initializeblank(testdict, " 2")
# printgrid(testdict)
# movenumber(testdict, " 2", findgridloc(solvedict, " 2"))
# printgrid(testdict)
